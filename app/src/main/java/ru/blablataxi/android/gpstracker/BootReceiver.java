package ru.blablataxi.android.gpstracker;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String action = intent.getAction();
        if(action.equalsIgnoreCase("android.intent.action.BOOT_COMPLETED"))
        {
            SharedPreferences settings = context.getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE);
            if(settings.getBoolean("enabled", false))
            {
                Log.d("BootReceiver", settings.getString("car_number", ""));
                Intent sIntent = new Intent(context, LocationService.class);
                sIntent.putExtra("car_number", settings.getString("car_number", ""));
                context.startService(sIntent);
            }
        }
    }
}

