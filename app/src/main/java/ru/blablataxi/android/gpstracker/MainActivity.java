package ru.blablataxi.android.gpstracker;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    protected static final String TAG = "location-updates-sample";

    public static final String APP_SETTINGS = "settings";


    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";


    protected Button mStartUpdatesButton;
    protected Button mStopUpdatesButton;
    protected TextView mLastUpdateTimeTextView;
    protected TextView mLatitudeTextView;
    protected TextView mLongitudeTextView;
    protected EditText mCarNumber;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    private String mLastUpdateTime;
    private SharedPreferences mSettings;
    private Intent mLocationService;
    private String longitude;
    private String latitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.main_activity);

        mStartUpdatesButton = (Button) findViewById(R.id.start_updates_button);
        mStopUpdatesButton = (Button) findViewById(R.id.stop_updates_button);
        mLatitudeTextView = (TextView) findViewById(R.id.latitude_text);
        mLongitudeTextView = (TextView) findViewById(R.id.longitude_text);
        mLastUpdateTimeTextView = (TextView) findViewById(R.id.last_update_time_text);
        mCarNumber = (EditText) findViewById(R.id.car_number);

        mLastUpdateTime = "";
        mSettings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
        mRequestingLocationUpdates = false;

        updateValuesFromBundle(savedInstanceState);

        if(mSettings.getBoolean("enabled", false))
        {
            setButtonsEnabledState();
            startUpdatesButtonHandler(mStartUpdatesButton);
        }
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        Log.i(TAG, "Updating values from bundle");
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
                setButtonsEnabledState();
            }
        }
    }

    public void startUpdatesButtonHandler(View view) {
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            setButtonsEnabledState();
            PendingIntent pi;
            pi = createPendingResult(1, new Intent(), 0);
            mLocationService = new Intent(this, LocationService.class);
            mLocationService.putExtra("car_number", mCarNumber.getText().toString());
            mLocationService.putExtra("pendingIntent", pi);
            startService(mLocationService);
            Log.d(TAG, "startUpdatesButtonHandler");
        }
    }

    public void stopUpdatesButtonHandler(View view) {
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            setButtonsEnabledState();
            this.stopService(mLocationService);
        }
    }

    private void setButtonsEnabledState() {
        if (mRequestingLocationUpdates) {
            mStartUpdatesButton.setEnabled(false);
            mStopUpdatesButton.setEnabled(true);
        } else {
            mStartUpdatesButton.setEnabled(true);
            mStopUpdatesButton.setEnabled(false);
        }
    }

    /**
     * Updates the latitude, the longitude, and the last location time in the UI.
     */
    private void updateUI() {
        if(longitude != null && latitude != null)
        {
            mLatitudeTextView.setText(latitude);
            mLongitudeTextView.setText(longitude);
            mLastUpdateTimeTextView.setText(mLastUpdateTime);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        mCarNumber.setText(mSettings.getString("car_number", ""));
        longitude = mSettings.getString("longitude", "");
        latitude = mSettings.getString("latitude", "");
        mLastUpdateTime = mSettings.getString("date", "");
        updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("car_number", mCarNumber.getText().toString());
        editor.putBoolean("enabled", mRequestingLocationUpdates);
        editor.apply();
    }


    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 200)
        {
            longitude = data.getStringExtra("longitude");
            latitude = data.getStringExtra("latitude");
            mLastUpdateTime = data.getStringExtra("date");
            updateUI();

        }
    }
}

